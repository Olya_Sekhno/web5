function calculation(){
    let kol = document.getElementById("kol").value;
    let sto = document.getElementById("sto").value;
    if((kol.match(/^[0-9]*[.]?[0-9]+$/) === null) || (sto.match(/^[0-9]*[.]?[0-9]+$/) === null)){
        document.getElementById("result").innerHTML="Итог: Oшибка";
        alert("Что-то не так...(если Вы использовали вещественное число, запишите его через точку)");
    }
    else{
        let a = (kol * sto).toFixed(2);
        document.getElementById("result").innerHTML="Итог: " + a;
    }
}
window.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button");
    b.addEventListener("click", calculation);
});
